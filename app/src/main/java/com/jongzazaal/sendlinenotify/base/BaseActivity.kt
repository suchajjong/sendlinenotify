package com.jongzazaal.sendlinenotify.base

import android.os.Bundle
import com.jongzazaal.sendlinenotify.local.preference.UserPreference
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

open class BaseActivity(): DaggerAppCompatActivity() {
    @Inject
    lateinit var userPreference: UserPreference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

}