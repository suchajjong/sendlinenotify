package com.jongzazaal.sendlinenotify.base

import android.os.Bundle
import com.jongzazaal.sendlinenotify.local.preference.UserPreference
import dagger.android.support.DaggerFragment
import javax.inject.Inject

open class BaseFragment(): DaggerFragment() {
    @Inject
    lateinit var userPreference: UserPreference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

}