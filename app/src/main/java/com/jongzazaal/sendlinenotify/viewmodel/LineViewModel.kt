package com.jongzazaal.sendlinenotify.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.jongzazaal.core.model.base.ErrorModel
import com.jongzazaal.sendlinenotify.model.line.LineMsgModel
import com.jongzazaal.sendlinenotify.repository.LineRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import javax.inject.Inject

class LineViewModel (app: Application, var mRepository: LineRepository): ViewModel() {
    /**
     * Factory for constructing DevByteViewModel with parameter
     */
    class Factory(val app: Application, val mRepository: LineRepository) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(LineViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return LineViewModel(app, mRepository ) as T
            }
            throw IllegalArgumentException("Unable to construct viewmodel")
        }
    }

    /**
     * This is the job for all coroutines started by this ViewModel.
     *
     * Cancelling this job will cancel all coroutines started by this ViewModel.
     */
    private val viewModelJob = SupervisorJob()

    /**
     * This is the main scope for all coroutines launched by MainViewModel.
     *
     * Since we pass viewModelJob, you can cancel all coroutines launched by uiScope by calling
     * viewModelJob.cancel()
     */
    private val viewModelScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    /**
     * The data source this ViewModel will fetch results from.
     */



    /**
     * A playlist of videos that can be shown on the screen. Views should use this to get access
     * to the data.
     */

    val sendMsg: LiveData<LineMsgModel>
        get() = mRepository.sendMsg

    val error: LiveData<ErrorModel>
        get() = mRepository.error

    /**
     * Refresh data from the repository. Use a coroutine launch to run in a
     * background thread.
     */
    fun sendMsg(msg: String) {
        viewModelScope.launch {
            mRepository.sendMsg(msg)
        }
    }

    fun sendMsg(msg: String, lineToken: String) {
        viewModelScope.launch {
            mRepository.sendMsg(msg, lineToken)
        }
    }

}