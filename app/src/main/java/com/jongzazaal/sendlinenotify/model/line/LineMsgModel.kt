package com.jongzazaal.sendlinenotify.model.line

import com.jongzazaal.core.model.base.BaseResult
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class LineMsgModel(
    @field:Json(name = "msg")
    val msg: String? = ""
)
fun LineMsgModel.toBaseResult(): BaseResult<LineMsgModel> {
    return BaseResult<LineMsgModel>(_data = this)
}