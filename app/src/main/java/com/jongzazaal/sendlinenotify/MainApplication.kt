package com.jongzazaal.sendlinenotify

import android.content.Context
import androidx.multidex.MultiDex
import com.jongzazaal.core.di.koin.coreModule
import com.jongzazaal.sendlinenotify.di.DaggerAppComponent
import com.jongzazaal.sendlinenotify.di.koin.appModule
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.GlobalContext.startKoin

class MainApplication: DaggerApplication()  {
    private val applicationInjector = DaggerAppComponent.builder().application(this).build()
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> = applicationInjector
//    @Inject
//    lateinit var androidInjector: DispatchingAndroidInjector<Any>
//
//    override fun androidInjector(): AndroidInjector<Any> {
//        return androidInjector
//    }
//    override fun activityInjector(): AndroidInjector<Activity> = androidInjector

    override fun onCreate() {
        super.onCreate()
        ContexterManager.getInstance().setApplicationContext(this)
        ContexterManager.getInstance().setApplication(this)
        startKoin{
            androidLogger()
            androidContext(this@MainApplication)
            modules(appModule, coreModule)
        }
//        DaggerApp

//        AppInjector.init(this)
    }


    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

//    override fun androidInjector(): AndroidInjector<Any> {
//        return dispatchingAndroidInjector
//    }
}