package com.jongzazaal.sendlinenotify.network

import com.jongzazaal.sendlinenotify.BuildConfig
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonReader
import com.squareup.moshi.Moshi
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.io.IOException

object RetrofitManager {
    private val CONTROL_INTERCEPTOR = object : Interceptor {
        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain): Response {
            val originalRequest = chain.request()
            val request = originalRequest.newBuilder()
            request.addHeader("Content-Type", "application/json")
            request.addHeader("Accept", "application/json")
//            if (UserManager.token != null){
//                request.addHeader("Authorization", "Bearer ${UserManager.token}")
//
//            }

            val response = chain.proceed(request.build())
            return response.newBuilder()
                .removeHeader("Pragma")
                .header("Cache-Control", "public, max-age=2419220")
                .build()
        }
    }
    private val CONTROL_INTERCEPTOR_WITHOUT_TOKEN = object : Interceptor {
        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain): Response {
            val originalRequest = chain.request()
            val request = originalRequest.newBuilder()
            request.addHeader("Content-Type", "application/json")
            request.addHeader("Accept", "application/json")
            val response = chain.proceed(request.build())
            return response.newBuilder()
                .removeHeader("Pragma")
                .header("Cache-Control", "public, max-age=2419220")
                .build()
        }
    }
    // Configure retrofit to parse JSON and use coroutines
    private fun log(withOutToken: Boolean = false): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        if (BuildConfig.DEBUG){
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

        }
        else{
            interceptor.setLevel(HttpLoggingInterceptor.Level.NONE)

        }

        val okHttpBuilder =
            if(BuildConfig.IS_PRODUCTION){
                OkHttpClient.Builder()}
//            else if (ShareManager.unsafeOkHttpEnable == true){ UnsafeOkHttpClient.getUnsafeOkHttpClient().newBuilder() }
            else{ OkHttpClient.Builder()}
//            if(BuildConfig.IS_PRODUCTION){
//                OkHttpClient.Builder()}
//            else{ UnsafeOkHttpClient.getUnsafeOkHttpClient().newBuilder()}


        if (withOutToken){
            return okHttpBuilder
                .addInterceptor(interceptor)
                .addNetworkInterceptor(CONTROL_INTERCEPTOR_WITHOUT_TOKEN)
                .build()
        }
        else{
            return okHttpBuilder
                .addInterceptor(interceptor)
                .addNetworkInterceptor(CONTROL_INTERCEPTOR)
                .build()
        }

    }
    object NULL_TO_EMPTY_STRING_ADAPTER {
        @FromJson
        fun fromJson(reader: JsonReader): String {
            if (reader.peek() != JsonReader.Token.NULL) {
                return reader.nextString()
            }
            reader.nextNull<Unit>()
            return ""
        }
    }
    fun getMoshi(): Moshi {
        return Moshi.Builder()
//            .add(NULL_TO_EMPTY_STRING_ADAPTER)
//            .add(KotlinJsonAdapterFactory())
            .build()
    }
    private val retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_URL)
        .addConverterFactory(MoshiConverterFactory.create(getMoshi()))
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .client(log())
        .build()

    val network = retrofit.create(ServiceInterface::class.java)

}