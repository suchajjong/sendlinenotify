package com.jongzazaal.sendlinenotify.network

import com.jongzazaal.sendlinenotify.model.line.LineMsgModel
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface ServiceInterface {
    @POST("/line/send-msg")
    suspend fun sendMsg(@Header("line-token") token: String?, @Body model: LineMsgModel): LineMsgModel
}