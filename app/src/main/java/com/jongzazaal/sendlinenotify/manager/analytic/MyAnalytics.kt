package com.jongzazaal.sendlinenotify.manager.analytic

import android.os.Build
import android.util.Log
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase

object MyAnalytics {
    private var mFirebaseAnalytics: FirebaseAnalytics? = null

    private fun getFirebaseAnalyticsInstance(): FirebaseAnalytics {
        if (mFirebaseAnalytics == null) {
            mFirebaseAnalytics = Firebase.analytics
        }
        return mFirebaseAnalytics ?: Firebase.analytics

    }

    fun sendBuy(text: String) {

        //firebase
        getFirebaseAnalyticsInstance().logEvent("click_buy") {
            param("c_item", text)
        }

    }
}