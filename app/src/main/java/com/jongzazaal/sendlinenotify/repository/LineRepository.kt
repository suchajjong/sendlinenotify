package com.jongzazaal.sendlinenotify.repository

import androidx.lifecycle.MutableLiveData
import com.jongzazaal.core.model.base.Error
import com.jongzazaal.core.model.base.ErrorModel
import com.jongzazaal.sendlinenotify.model.line.LineMsgModel
import com.jongzazaal.sendlinenotify.model.line.toBaseResult
import com.jongzazaal.core.network.APICallHandler
import com.jongzazaal.sendlinenotify.network.RetrofitManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class LineRepository(val retrofit: RetrofitManager) {

    val sendMsg: MutableLiveData<LineMsgModel> = MutableLiveData()
    val error: MutableLiveData<ErrorModel> = MutableLiveData()

    suspend fun sendMsg(msg: String) {
        withContext(Dispatchers.IO) {
//            val model = APICallHandler.handlerMessageList{ RetrofitManager.network.getAdsList().toBaseResultList()}
//
//            if (model.error.error == Error.SUCCESS){
//                adsList.postValue(model.data)
//            }else{
//                error.postValue(model.error)
//            }

        }
    }
    suspend fun sendMsg(msg: String, lineToken: String) {
        withContext(Dispatchers.IO) {
            val model = APICallHandler.handlerMessage{ retrofit.network.sendMsg(lineToken, LineMsgModel(msg)).toBaseResult()}

            if (model.error.error == Error.SUCCESS){
                sendMsg.postValue(model.data)
            }else{
                error.postValue(model.error)
            }
        }
    }
}