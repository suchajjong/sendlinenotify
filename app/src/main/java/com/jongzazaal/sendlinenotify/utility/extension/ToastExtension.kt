package com.jongzazaal.sendlinenotify.utility.extension

import android.widget.Toast
import com.jongzazaal.sendlinenotify.BuildConfig
import com.jongzazaal.sendlinenotify.ContexterManager

fun Any.showToastDebug() {
    if (BuildConfig.IS_PRODUCTION){

    }
    else{
        Toast.makeText(ContexterManager.getInstance().getApplicationContext(), "${this}(debug)", Toast.LENGTH_SHORT).show()
    }
}

fun Any.showToast() {
    Toast.makeText(ContexterManager.getInstance().getApplicationContext(), "${this}", Toast.LENGTH_SHORT).show()
}
