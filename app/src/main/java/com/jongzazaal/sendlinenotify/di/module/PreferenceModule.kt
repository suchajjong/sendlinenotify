package com.jongzazaal.sendlinenotify.di.module

import android.app.Application
import com.jongzazaal.sendlinenotify.local.preference.UserPreference
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

//@Module
//abstract class PreferenceModule {
//
//    @Binds
//    @Singleton
//    abstract fun provideUserPreference(mainApplication: MainApplication):UserPreference
//}

@Module
class PreferenceModule {

    @Provides
    @Singleton
    fun provideUserPreference(mainApplication: Application):UserPreference{
        return UserPreference(mainApplication.applicationContext)
    }

}