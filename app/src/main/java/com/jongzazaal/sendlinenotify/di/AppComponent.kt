package com.jongzazaal.sendlinenotify.di

import android.app.Application
import com.jongzazaal.sendlinenotify.MainApplication
import com.jongzazaal.sendlinenotify.di.module.ActivityModule
import com.jongzazaal.sendlinenotify.di.module.NetworkModule
import com.jongzazaal.sendlinenotify.di.module.PreferenceModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        PreferenceModule::class,
        ActivityModule::class,
        NetworkModule::class
    ]
)
interface AppComponent : AndroidInjector<MainApplication>{
    @Component.Builder
    interface Builder{
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent

    }

    override fun inject(mainApplication: MainApplication)

}