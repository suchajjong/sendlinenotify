package com.jongzazaal.sendlinenotify.di.module

import com.jongzazaal.sendlinenotify.base.BaseActivity
import com.jongzazaal.sendlinenotify.base.BaseFragment
import com.jongzazaal.sendlinenotify.module.MainActivity
import com.jongzazaal.sendlinenotify.module.SettingActivity
import com.jongzazaal.sendlinenotify.module.ui.main.SettingFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class ActivityModule {

    @ContributesAndroidInjector()
    abstract fun contributeBaseActivity(): BaseActivity

    @ContributesAndroidInjector()
    abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector()
    abstract fun contributeSettingActivity(): SettingActivity

    @ContributesAndroidInjector()
    abstract fun contributeBaseFragment(): BaseFragment

    @ContributesAndroidInjector()
    abstract fun contributeSettingFragment(): SettingFragment
}