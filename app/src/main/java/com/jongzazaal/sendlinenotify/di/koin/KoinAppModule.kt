package com.jongzazaal.sendlinenotify.di.koin

import com.jongzazaal.sendlinenotify.network.RetrofitManager
import com.jongzazaal.sendlinenotify.repository.LineRepository
import com.jongzazaal.sendlinenotify.viewmodel.LineViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    single { RetrofitManager }
    single { LineRepository(get()) }
//    single { LineViewModel(androidApplication(), get()) }
    // MyViewModel ViewModel
    viewModel<LineViewModel>() { LineViewModel(androidApplication(), get()) }
}