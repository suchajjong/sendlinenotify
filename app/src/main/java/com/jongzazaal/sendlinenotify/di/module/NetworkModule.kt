package com.jongzazaal.sendlinenotify.di.module

import android.app.Application
import com.jongzazaal.sendlinenotify.network.RetrofitManager
import com.jongzazaal.sendlinenotify.repository.LineRepository
import com.jongzazaal.sendlinenotify.viewmodel.LineViewModel
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideRetrofit(): RetrofitManager {
        return RetrofitManager
    }

    @Provides
    @Singleton
    fun provideLineRepository(retrofit: RetrofitManager): LineRepository {
       return LineRepository(retrofit)
    }

    @Provides
    @Singleton
    fun provideLineViewModel(app: Application, repo: LineRepository): LineViewModel {
        return LineViewModel(app, repo)
    }

}