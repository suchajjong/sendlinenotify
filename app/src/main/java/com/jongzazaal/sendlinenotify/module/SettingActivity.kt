package com.jongzazaal.sendlinenotify.module

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.jongzazaal.sendlinenotify.R
import com.jongzazaal.sendlinenotify.base.BaseActivity
import com.jongzazaal.sendlinenotify.databinding.SettingActivityBinding
import com.jongzazaal.sendlinenotify.module.ui.main.SettingFragment

class SettingActivity : BaseActivity() {
    private val binding: SettingActivityBinding by lazy { SettingActivityBinding.inflate(layoutInflater) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, SettingFragment.newInstance())
                .commitNow()
        }
    }


    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, SettingActivity::class.java).apply {

            })
        }
    }
}