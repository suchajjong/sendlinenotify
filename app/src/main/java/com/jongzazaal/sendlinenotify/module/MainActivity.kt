package com.jongzazaal.sendlinenotify.module

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfigSettings
import com.jongzazaal.sendlinenotify.BuildConfig
import com.jongzazaal.sendlinenotify.R
import com.jongzazaal.sendlinenotify.base.BaseActivity
import com.jongzazaal.core.base.BaseCommon
import com.jongzazaal.core.extension.afterTextChangedCustom
import com.jongzazaal.core.extension.gone
import com.jongzazaal.core.extension.visible
import com.jongzazaal.core.local.CorePreference
import com.jongzazaal.sendlinenotify.databinding.ActivityMainBinding
import com.jongzazaal.sendlinenotify.manager.analytic.MyAnalytics
import com.jongzazaal.sendlinenotify.repository.LineRepository
import com.jongzazaal.sendlinenotify.utility.extension.showToast
import com.jongzazaal.sendlinenotify.viewmodel.LineViewModel
import javax.inject.Inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.android.ext.android.inject

class MainActivity: BaseActivity(), BaseCommon {

    private val binding: ActivityMainBinding by lazy { ActivityMainBinding.inflate(layoutInflater) }
//    private val viewModel: LineViewModel by lazy {
//        ViewModelProvider(this, LineViewModel.Factory(application, lineRepo)).get("MainActivity", LineViewModel::class.java)
//    }

    @Inject
    lateinit var viewModel: LineViewModel
//    @Inject
//    lateinit var lineRepo: LineRepository

//    val viewModel: LineViewModel by viewModel()
    private val corePreference: CorePreference by inject()

    private lateinit var mFirebaseRemoteConfig: FirebaseRemoteConfig


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        initView()
        initListener()
        initViewModel()
        firstCreate()
        userPreference.saveUserName("123")
        corePreference.saveUserName("c123")
        Log.d("tag2", "u: ${userPreference.getUserName()}")
        Log.d("tag2", "core: ${corePreference.getUserName()}")

        setupRemoteConfig()
    }

    private fun setupRemoteConfig(){

        val remoteConfig   = remoteConfigSettings {
            minimumFetchIntervalInSeconds = if (BuildConfig.IS_PRODUCTION) {3600}else{0}
        }

        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance()
        mFirebaseRemoteConfig.setDefaultsAsync(R.xml.remote_config_defaults)
        mFirebaseRemoteConfig.setConfigSettingsAsync(remoteConfig)

        mFirebaseRemoteConfig.fetchAndActivate().addOnCompleteListener {
            if (it.isSuccessful){
//                mFirebaseRemoteConfig.fetchAndActivate()
                setConfig()
            }
        }

    }

    private fun setConfig(){
        val promotion = mFirebaseRemoteConfig.getBoolean("is_promotion_on")
        val price = mFirebaseRemoteConfig.getLong("price")
        val discount = mFirebaseRemoteConfig.getLong("discount")
        val buy_btn = mFirebaseRemoteConfig.getString("buy_btn")

        binding.buy.text = buy_btn


        binding.config.text =  "promotion:$promotion\nprice:$price\ndiscount:$discount"
    }

    override fun firstCreate() {
        checkSendBtn()
    }

    override fun initView() {
        userPreference.getUserLineToken()?.let { token ->
            binding.host.setText(token)
        }
    }

    override fun initListener() {
        binding.send.setOnClickListener {
            showLoading()
            viewModel.sendMsg(binding.msg.text.toString(), binding.host.text.toString())
            userPreference.saveUserLineToken(binding.host.text.toString())
        }
        binding.msg.afterTextChangedCustom {
            checkSendBtn()
        }
        binding.host.afterTextChangedCustom {
            checkSendBtn()
        }
        binding.setting.setOnClickListener {
            openSetting(this)
        }
        binding.covid.setOnClickListener {
            openCovid(this)
        }
        binding.buy.setOnClickListener {
            MyAnalytics.sendBuy(binding.buy.text.toString())
        }
    }

    fun openSetting(context: Context){
        val intent = Intent()
        intent.setClassName(BuildConfig.APPLICATION_ID, "com.jongzazaal.sendlinenotify.setting.SettingAppActivity")
        startActivity(intent)
    }
    fun openCovid(context: Context){
        val intent = Intent()
        intent.setClassName(BuildConfig.APPLICATION_ID, "com.jongzazaal.covid.ShowCovidActivity")
        startActivity(intent)
    }

    override fun initViewModel() {
        viewModel.sendMsg.observe(this, Observer { result ->
            hideLoading()
            "send success".showToast()
        })
        viewModel.error.observe(this, Observer { result ->
            hideLoading()
            "error: ${result.error.name}".showToast()
        })
    }

    override fun showLoading() {
        binding.loading.visible()
    }

    override fun hideLoading() {
        binding.loading.gone()

    }

    private fun checkSendBtn(){
        binding.send.isEnabled = (binding.msg.text?:"").isNotEmpty() && (binding.host.text?:"").isNotEmpty()
    }

}