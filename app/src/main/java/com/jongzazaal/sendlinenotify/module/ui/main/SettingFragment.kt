package com.jongzazaal.sendlinenotify.module.ui.main

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jongzazaal.sendlinenotify.base.BaseFragment
import com.jongzazaal.sendlinenotify.databinding.MainFragmentBinding
import com.jongzazaal.sendlinenotify.local.preference.UserPreference
import com.jongzazaal.sendlinenotify.utility.extension.showToast
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class SettingFragment : BaseFragment() {

    private lateinit var binding: MainFragmentBinding

    companion object {
        fun newInstance() = SettingFragment()
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MainFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        userPreference.getUserName()?.showToast()
    }

}