package com.jongzazaal.sendlinenotify

import com.jongzazaal.sendlinenotify.AndroidTest
import com.jongzazaal.sendlinenotify.module.MainActivity
import com.jongzazaal.sendlinenotify.module.SettingActivity
import io.mockk.impl.annotations.MockK
import io.mockk.every
import io.mockk.verify
import org.junit.Before
import org.junit.Test

class NavigateTest: AndroidTest() {

    lateinit var mainActivity: MainActivity
    @Before
    fun setup() {
        mainActivity = MainActivity()
    }

    @Test
    fun `should forward user to login screen`() {
//        every { authenticator.userLoggedIn() } returns false

        mainActivity.openSetting(context())

//        verify(exactly = 1) { authenticator.userLoggedIn() }
        MainActivity::class shouldNavigateTo SettingActivity::class
    }

    @Test
    fun `should forward user to login screen 2`() {
//        every { authenticator.userLoggedIn() } returns false

        SettingActivity.start(context())

//        verify(exactly = 1) { authenticator.userLoggedIn() }
        MainActivity::class shouldNavigateTo SettingActivity::class
    }
}