package com.jongzazaal.customview

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.jongzazaal.customview.databinding.WidgetCustomLoadingBinding

class CustomLoading: LinearLayout {
    private lateinit var binding: WidgetCustomLoadingBinding

    @JvmOverloads
    constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
    )
            : super(context, attrs, defStyleAttr) {
        setup(context, attrs, defStyleAttr)
    }

    private fun setup(context: Context, attrs: AttributeSet?, defStyleAttr: Int) {
        binding = WidgetCustomLoadingBinding.inflate(LayoutInflater.from(getContext()), this, true)
    }

    override fun onVisibilityChanged(changedView: View, visibility: Int) {
        super.onVisibilityChanged(changedView, visibility)
        when(visibility){
            View.VISIBLE -> {}
            else -> {}
        }
    }
}