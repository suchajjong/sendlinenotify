package com.jongzazaal.customview

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatButton

class CustomButton: AppCompatButton {
//    var corner:Int = 0

//    var isPlaceHolder = false

    @JvmOverloads
    constructor(
        context: Context,
        attrs: AttributeSet,
        defStyleAttr: Int = R.attr.buttonStyle)
            : super(context, attrs, defStyleAttr) {
        initAttrs(context, attrs)
    }
    private fun initAttrs(context: Context, attrs: AttributeSet) {

    }
}