package com.jongzazaal.core.local

import android.content.Context
import android.content.SharedPreferences

class CorePreference(var context: Context) {
    companion object {
        private const val PREFERENCE_NAME = "core_preference"
        private const val KEY_ID = "key_id"
        private const val KEY_NAME = "key_name"
        private const val KEY_LINE_TOKEN = "key_line_token"

    }

    private val sharedPreferences: SharedPreferences =
        context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)


    fun saveUserName(name: String) {
        sharedPreferences.edit().putString(KEY_NAME, name).apply()
    }

    fun getUserName(): String? = sharedPreferences.getString(KEY_NAME, null)

    fun saveUserLineToken(lineToken: String) {
        sharedPreferences.edit().putString(KEY_LINE_TOKEN, lineToken).apply()
    }

    fun getUserLineToken(): String? = sharedPreferences.getString(KEY_LINE_TOKEN, null)
}