package com.jongzazaal.core.di.koin

import com.jongzazaal.core.local.CorePreference
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val coreModule = module {
    single { CorePreference(androidApplication()) }
}