package com.jongzazaal.core.model.base

import com.squareup.moshi.Json

data class BaseResultList<T>(
    @field:Json(name = "response_code") private val _responseCode: Int = -500,
    @field:Json(name = "response_msg") private val _responseMsg: String? = null,
    @field:Json(name = "result") private val _data: List<T>? = null,

    private var _error: ErrorModel = ErrorModel()


){
    val responseCode: Int
        get() = _responseCode

    val responseMsg: String
        get() = _responseMsg?:""

    val data: List<T>?
        get() = _data

    val error: ErrorModel
        get() = _error
}