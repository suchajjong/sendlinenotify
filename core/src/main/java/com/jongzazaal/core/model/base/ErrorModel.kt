package com.jongzazaal.core.model.base

import kotlinx.coroutines.CoroutineScope

data class ErrorModel(
    val code: String = "",
    val msg: String = "",
    val msgFromServer: String = "",
    val codeFromServer: Int? = null,
    val error: Error = Error.SUCCESS,
    private var _errorCall:(suspend CoroutineScope.() -> Unit)? = null

){
    var errorCall: (suspend CoroutineScope.() -> Unit)?
        get() = _errorCall
        set(value) { _errorCall = value }
}
enum class Error {
    NO_INTERNET_CONNECTION,//(e001)
    HTTP_EXCEPTION, // Network Code is not 200. (e002)
    TIMEOUT,//(e003)
    JSON_PAIR_FAIL, //(e004)
    SUCCESS,
    OTHER //(e005)
}