package com.jongzazaal.core.extension

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.widget.AppCompatEditText
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

fun View.gone() {
    this.visibility = View.GONE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.setOnSafeClickListener(
    onSafeClick: (View) -> Unit
) {
    setOnClickListener(SafeClickListener { v ->
        onSafeClick(v)
    })
}

class SafeClickListener (
    private val interval: Int = 2000,
    private val onSafeClick: (View) -> Unit
) : View.OnClickListener {

    private var lastClickTime: Long = 0L

    override fun onClick(v: View) {
        if (Date().time - lastClickTime < interval) {
            lastClickTime = Date().time
            return
        }
        lastClickTime = Date().time
        onSafeClick(v)
    }
}

fun AppCompatEditText.afterTextChangedCustom(delayMS: Long = 1, thaiOnly: Boolean = false, engOnly: Boolean = false, afterTextChanged: (String) -> Unit) {
    val handler = android.os.Handler()
    var runnable: Runnable? = null
    this.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            runnable?.let { handler.removeCallbacks(it) }
            if (thaiOnly){
                val ps: Pattern = Pattern.compile("^[ก-๙. ]+$")
                val ms: Matcher = ps.matcher(s)
                val bs: Boolean = ms.matches()
                if (!s.isNullOrEmpty() && !bs){
                    this@afterTextChangedCustom.setText(s.substring(0, s.length-1))

                }
            }

        }

        override fun afterTextChanged(s: Editable?) {
            runnable = Runnable {
                afterTextChanged.invoke(s.toString())
            }
            handler.postDelayed(runnable!!, delayMS)
        }
    })
}