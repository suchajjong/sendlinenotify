package com.jongzazaal.core.extension

import java.text.DecimalFormat

fun String?.toNumberComma(): String {
    if (this.isNullOrEmpty()|| this == "null"){
        return "null"
    }
    val str = this.replace(",", "")
    val formatter = DecimalFormat("###,###,###,##0")
    formatter.negativePrefix = "- "
    val yourFormattedString = formatter.format(str.toFloat())
    return yourFormattedString
}