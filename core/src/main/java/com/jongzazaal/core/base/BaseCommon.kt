package com.jongzazaal.core.base

interface BaseCommon {
    fun firstCreate()
    fun initView()
    fun initListener()
    fun initViewModel()
    fun showLoading()
    fun hideLoading()
}