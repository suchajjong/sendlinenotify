package com.jongzazaal.core.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

open class CoreBaseActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
}