package com.jongzazaal.core.extension

import org.junit.Assert.*

import org.junit.Test

class StringExtensionKtTest {

    @Test
    fun toNumberComma() {
        assertEquals("0".toNumberComma(), "0")

        assertEquals("1".toNumberComma(), "1")
        assertEquals("10".toNumberComma(), "10")
        assertEquals("100".toNumberComma(), "100")
        assertEquals("1000".toNumberComma(), "1,000")
        assertEquals("10000".toNumberComma(), "10,000")
        assertEquals("100000".toNumberComma(), "100,000")

        assertEquals("-1".toNumberComma(), "- 1")
        assertEquals("-10".toNumberComma(), "- 10")
        assertEquals("-100".toNumberComma(), "- 100")
        assertEquals("-1000".toNumberComma(), "- 1,000")
        assertEquals("-10000".toNumberComma(), "- 10,000")
        assertEquals("-100000".toNumberComma(), "- 100,000")

        assertEquals("1.0".toNumberComma(), "1")
        assertEquals("1.1".toNumberComma(), "1")
        assertEquals("1.5".toNumberComma(), "2")
        assertEquals("1.9".toNumberComma(), "2")

        assertEquals("null".toNumberComma(), "null")
        assertEquals("".toNumberComma(), "null")

    }
}