package com.jongzazaal.sendlinenotify.setting

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.jongzazaal.sendlinenotify.module.SettingActivity
import com.jongzazaal.sendlinenotify.setting.databinding.ActivitySettingAppBinding

class SettingAppActivity : AppCompatActivity() {
    private val binding: ActivitySettingAppBinding by lazy { ActivitySettingAppBinding.inflate(layoutInflater) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        binding.btn.setOnClickListener {
            SettingActivity.start(this)
        }
    }
}