package com.jongzazaal.covid

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.jongzazaal.core.base.BaseCommon
import com.jongzazaal.covid.databinding.FragmentSettingBinding

/**
 * A simple [Fragment] subclass.
 * Use the [SettingFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SettingFragment : Fragment(), BaseCommon {
    private lateinit var binding: FragmentSettingBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {}
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentSettingBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initListener()
        firstCreate()
    }

    override fun firstCreate() {

    }

    override fun initView() {
        arguments?.let { arguments ->
            val args = SettingFragmentArgs.fromBundle(arguments)
            val api = args.api
            setRadio(api)
        }

    }

    override fun initListener() {
        binding.radioApi.setOnCheckedChangeListener { radioGroup, i ->
            when(i){
                R.id.api_moph -> {}
                else -> {}
            }
        }
        binding.submit.setOnClickListener {
            binding.root.findNavController().previousBackStackEntry?.savedStateHandle?.set(
                HomeFragment.API_KEY, getApiFromRadio()
            )
            binding.root.findNavController().popBackStack()

        }
    }
    private fun getApiFromRadio():String{
        return when(binding.radioApi.checkedRadioButtonId){
            R.id.api_moph -> {HomeFragment.API_MOPH}
            else -> {HomeFragment.API_EASY_SUNDAY}
        }
    }
    private fun setRadio(check: String){
        val radioCheck = when(check){
            HomeFragment.API_MOPH -> { R.id.api_moph}
            else -> {R.id.api_easy_sunday}
        }
        binding.radioApi.check(radioCheck)
    }

    override fun initViewModel() {}

    override fun showLoading() {}

    override fun hideLoading() {}

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            SettingFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }
}