package com.jongzazaal.covid.model


import com.jongzazaal.core.model.base.BaseResult
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CovidEasySunday(
    @Json(name = "active")
    var active: Int? = 0,
    @Json(name = "activePerOneMillion")
    var activePerOneMillion: Double? = 0.0,
    @Json(name = "cases")
    var cases: Int? = 0,
    @Json(name = "casesPerOneMillion")
    var casesPerOneMillion: Int? = 0,
    @Json(name = "Confirmed")
    var confirmed: Int? = 0,
    @Json(name = "continent")
    var continent: String? = null,
    @Json(name = "country")
    var country: String? = null,
    @Json(name = "critical")
    var critical: Int? = 0,
    @Json(name = "criticalPerOneMillion")
    var criticalPerOneMillion: Double? = 0.0,
    @Json(name = "deaths")
    var deaths: Int? = 0,
    @Json(name = "Deaths")
    var deaths_2: Int? = 0,
    @Json(name = "deathsPerOneMillion")
    var deathsPerOneMillion: Int? = 0,
    @Json(name = "DevBy")
    var devBy: String? = null,
    @Json(name = "Hospitalized")
    var hospitalized: Int? = 0,
    @Json(name = "NewConfirmed")
    var newConfirmed: Int? = 0,
    @Json(name = "NewDeaths")
    var newDeaths: Int? = 0,
    @Json(name = "NewHospitalized")
    var newHospitalized: Int? = 0,
    @Json(name = "NewRecovered")
    var newRecovered: Int? = 0,
    @Json(name = "oneCasePerPeople")
    var oneCasePerPeople: Int? = 0,
    @Json(name = "oneDeathPerPeople")
    var oneDeathPerPeople: Int? = 0,
    @Json(name = "oneTestPerPeople")
    var oneTestPerPeople: Int? = 0,
    @Json(name = "population")
    var population: Int? = 0,
    @Json(name = "recovered")
    var recovered: Int? = 0,
    @Json(name = "Recovered")
    var recovered_2: Int? = 0,
    @Json(name = "recoveredPerOneMillion")
    var recoveredPerOneMillion: Double? = 0.0,
    @Json(name = "tests")
    var tests: Int? = 0,
    @Json(name = "testsPerOneMillion")
    var testsPerOneMillion: Int? = 0,
    @Json(name = "todayCases")
    var todayCases: Int? = 0,
    @Json(name = "todayDeaths")
    var todayDeaths: Int? = 0,
    @Json(name = "todayRecovered")
    var todayRecovered: Int? = 0,
    @Json(name = "UpdateDate")
    var updateDate: String? = null,
    @Json(name = "updated")
    var updated: Long? = 0L
){
    fun getToday():CovidToday{
        return CovidToday(
            newCase = todayCases,
            totalCase = confirmed,
            newRecovered = todayRecovered,
            totalRecovered = recovered,
            newHospitalized = null,
            totalHospitalized = null,
            newDeaths = todayDeaths,
            totalDeaths = deaths,
            UpdateDate = updateDate,
            powerBy = "EasySunday",
        )
    }
}
fun CovidEasySunday.toBaseResult(): BaseResult<CovidEasySunday> {
    return BaseResult<CovidEasySunday>(_data = this)
}