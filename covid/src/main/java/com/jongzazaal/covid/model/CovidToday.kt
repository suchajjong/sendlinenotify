package com.jongzazaal.covid.model

data class CovidToday(
    var newCase: Int? = null,
    var totalCase: Int? = null,
    var newRecovered: Int? = null,
    var totalRecovered: Int? = null,
    var newHospitalized: Int? = null,
    var totalHospitalized: Int? = null,
    var newDeaths: Int? = null,
    var totalDeaths: Int? = null,
    var UpdateDate: String? = null,
    var powerBy: String? = null



)