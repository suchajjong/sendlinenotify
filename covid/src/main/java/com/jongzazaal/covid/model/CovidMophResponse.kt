package com.jongzazaal.covid.model


import com.jongzazaal.core.model.base.BaseResultList
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CovidMophResponse(
    @Json(name = "new_case")
    var newCase: Int? = null,
    @Json(name = "new_case_excludeabroad")
    var newCaseExcludeabroad: Int? = null,
    @Json(name = "new_death")
    var newDeath: Int? = null,
    @Json(name = "new_recovered")
    var newRecovered: Int? = null,
    @Json(name = "total_case")
    var totalCase: Int? = null,
    @Json(name = "total_case_excludeabroad")
    var totalCaseExcludeabroad: Int? = null,
    @Json(name = "total_death")
    var totalDeath: Int? = null,
    @Json(name = "total_recovered")
    var totalRecovered: Int? = null,
    @Json(name = "txn_date")
    var txnDate: String? = null,
    @Json(name = "update_date")
    var updateDate: String? = null
){
    fun getToday():CovidToday{
        return CovidToday(
            newCase = newCase,
            totalCase = totalCase,
            newRecovered = newRecovered,
            totalRecovered = totalRecovered,
            newHospitalized = null,
            totalHospitalized = null,
            newDeaths = newDeath,
            totalDeaths = totalDeath,
            UpdateDate = updateDate,
            powerBy = "กรมควบคุมโรค",
        )
    }
}

fun List<CovidMophResponse>.toBaseResultList(): BaseResultList<CovidMophResponse> {
    return BaseResultList<CovidMophResponse>(_data = this)
}