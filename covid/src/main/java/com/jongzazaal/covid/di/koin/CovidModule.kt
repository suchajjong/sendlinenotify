package com.jongzazaal.covid.di.koin

import com.jongzazaal.core.network.RetrofitManager
import com.jongzazaal.covid.repository.CovidRepository
import com.jongzazaal.covid.viewmodel.CovidViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val covidModule = module {
    single { RetrofitManager }
    single { CovidRepository(get()) }
//    single { LineViewModel(androidApplication(), get()) }
    // MyViewModel ViewModel
    viewModel<CovidViewModel>() { CovidViewModel(androidApplication(), get()) }
}