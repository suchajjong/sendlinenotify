package com.jongzazaal.covid

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.jongzazaal.core.base.CoreBaseActivity
import com.jongzazaal.covid.databinding.ActivityShowCovidBinding

class ShowCovidActivity : CoreBaseActivity() {

    private val binding: ActivityShowCovidBinding by lazy { ActivityShowCovidBinding.inflate(layoutInflater) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
//        corePreference.saveUserName("456")
//        Log.d("tag3", corePreference.getUserName()?:"-")
//        corePreference.getUserName()?.let {
//            "covid name = it"
//        }
    }

    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, ShowCovidActivity::class.java).apply {

            })
        }
    }
}