package com.jongzazaal.covid.customView

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.appcompat.widget.LinearLayoutCompat
import com.jongzazaal.core.extension.toNumberComma
import com.jongzazaal.covid.R
import com.jongzazaal.covid.databinding.WidgetCovidBinding
import com.jongzazaal.covid.model.CovidToday

class CovidWidget: LinearLayoutCompat {
    private lateinit var binding: WidgetCovidBinding

    private var eventRefreshClickListener: (() -> Unit)? = null
    fun setOnRefreshClickListener(listener: () -> Unit) {
        eventRefreshClickListener = listener
    }

    @JvmOverloads
    constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
    )
            : super(context, attrs, defStyleAttr) {
        setupView(context, attrs, defStyleAttr)
    }
    private fun setupView(context: Context, attrs: AttributeSet?, defStyleAttr: Int) {
        binding = WidgetCovidBinding.inflate(LayoutInflater.from(getContext()), this, true)
        binding.button.setOnClickListener {
            setLoading()
            eventRefreshClickListener?.invoke()
        }
    }

    fun setData(model: CovidToday){
        binding.textViewUpdateDate.text = context.getString(R.string.UpdateDate).format(model.UpdateDate)
        binding.textViewConfirmed.text = model.totalCase.toString().toNumberComma()
        binding.textViewNewConfirmed.text = context.getString(R.string.updatePlus).format(model.newCase.toString().toNumberComma())
        binding.textViewRecovered.text = model.totalRecovered.toString().toNumberComma()
        binding.textViewNewRecovered.text = context.getString(R.string.updatePlus).format(model.newRecovered.toString().toNumberComma())
        binding.textViewHospitalized.text = model.totalHospitalized.toString().toNumberComma()
        binding.textViewNewHospitalized.text = context.getString(R.string.update).format(model.newHospitalized.toString().toNumberComma())
        binding.textViewDeaths.text = model.totalDeaths.toString().toNumberComma()
        binding.textViewNewDeaths.text = context.getString(R.string.updatePlus).format(model.newDeaths.toString().toNumberComma())

        binding.powerBy.text = context.getString(R.string.powerBy).format(model.powerBy)
    }

    private fun setLoading(){
        binding.textViewUpdateDate.text = context.getString(R.string.loading)

    }
    fun setError(){
        binding.textViewUpdateDate.text = context.getString(R.string.error)

    }


}