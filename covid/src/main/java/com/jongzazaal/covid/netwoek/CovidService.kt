package com.jongzazaal.covid.netwoek

import com.jongzazaal.covid.model.CovidEasySunday
import com.jongzazaal.covid.model.CovidMophResponse
import retrofit2.http.GET
//import retrofit2.http.POST

interface CovidService {
    @GET("/covid-19/getTodayCases.json")
    suspend fun getTodayEasySunday(): CovidEasySunday

    @GET("/api/Cases/today-cases-all")
    suspend fun getToday(): List<CovidMophResponse>
}