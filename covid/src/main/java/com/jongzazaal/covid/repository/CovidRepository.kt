package com.jongzazaal.covid.repository

import androidx.lifecycle.MutableLiveData
import com.jongzazaal.core.model.base.Error
import com.jongzazaal.core.model.base.ErrorModel
import com.jongzazaal.core.network.APICallHandler
import com.jongzazaal.core.network.RetrofitManager
import com.jongzazaal.covid.model.CovidToday
import com.jongzazaal.covid.model.toBaseResult
import com.jongzazaal.covid.model.toBaseResultList
import com.jongzazaal.covid.netwoek.CovidService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CovidRepository(val retrofit: RetrofitManager) {

    val covidTodayModel: MutableLiveData<CovidToday> = MutableLiveData()
    val error: MutableLiveData<ErrorModel> = MutableLiveData()

    suspend fun getCovidTodayEasySunday() {
        withContext(Dispatchers.IO) {
            val model = APICallHandler.handlerMessage{ retrofit.setBaseUrl("https://static.easysunday.com").create(CovidService::class.java).getTodayEasySunday().toBaseResult()}

            if (model.error.error == Error.SUCCESS){
                covidTodayModel.postValue(model.data?.getToday())

            }else{
                error.postValue(model.error)
            }
        }
    }

    suspend fun getCovidToday() {
        withContext(Dispatchers.IO) {
            val model = APICallHandler.handlerMessageList{ retrofit.setBaseUrl("https://covid19.ddc.moph.go.th").create(CovidService::class.java).getToday().toBaseResultList()}

            if (model.error.error == Error.SUCCESS){
                covidTodayModel.postValue(model.data?.firstOrNull()?.getToday())

            }else{
                error.postValue(model.error)
            }
        }
    }
}