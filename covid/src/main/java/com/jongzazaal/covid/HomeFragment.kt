package com.jongzazaal.covid

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.jongzazaal.core.base.BaseCommon
import com.jongzazaal.core.base.CoreBaseFragment
import com.jongzazaal.core.extension.gone
import com.jongzazaal.core.extension.visible
import com.jongzazaal.core.local.CorePreference
import com.jongzazaal.covid.databinding.FragmentHomeBinding
import com.jongzazaal.covid.di.koin.covidModule
import com.jongzazaal.covid.viewmodel.CovidViewModel
import com.jongzazaal.sendlinenotify.utility.extension.showToast
import org.koin.core.context.loadKoinModules
import org.koin.core.context.unloadKoinModules
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.android.ext.android.inject

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : CoreBaseFragment(), BaseCommon {
    private lateinit var binding: FragmentHomeBinding

    private var api = API_MOPH
//    private val viewModel: CovidViewModel by lazy {
//        val mRepo = CovidRepository(RetrofitManager)
//        ViewModelProvider(this, CovidViewModel.Factory(requireActivity().application, mRepo)).get("HomeFragment", CovidViewModel::class.java)
//    }
    val viewModel: CovidViewModel by viewModel()
    private val corePreference: CorePreference by inject()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadKoinModules(covidModule)
    }

    override fun onDestroy() {
        super.onDestroy()
        unloadKoinModules(covidModule)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initListener()
        initViewModel()
        firstCreate()
    }

    private fun refreshCovidData(){
        showLoading()
        if (api == API_MOPH){
            viewModel.getCovidToday()
        }
        else{
            viewModel.getCovidTodayEasySunday()

        }

    }

    override fun firstCreate() {
        refreshCovidData()
        Log.d("tag2", "core covid: ${corePreference.getUserName()}")

    }

    override fun initView() {}

    override fun initListener() {
        binding.covidWidget.setOnRefreshClickListener { refreshCovidData() }
        binding.settingApi.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragmentToSettingFragment(api)
            it.findNavController().navigate(action)
        }
        binding.root.findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData<String>(API_KEY)
            ?.observe(viewLifecycleOwner, Observer { result ->
                this.api = result
                refreshCovidData()
            })
    }

    override fun showLoading() {binding.loading.visible()}

    override fun hideLoading() {binding.loading.gone()}

    override fun initViewModel(){
        viewModel.covidTodayModel.observe(viewLifecycleOwner, Observer { result ->
            hideLoading()
            binding.covidWidget.setData(result)
        })
        viewModel.error.observe(viewLifecycleOwner, Observer { result ->
            hideLoading()
            binding.covidWidget.setError()
            "error".showToast()
        })
    }

    companion object {
        const val API_KEY = "api_key"
        const val API_MOPH = "api_moph"
        const val API_EASY_SUNDAY = "api_easy_sunday"

        @JvmStatic
        fun newInstance() =
            HomeFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}