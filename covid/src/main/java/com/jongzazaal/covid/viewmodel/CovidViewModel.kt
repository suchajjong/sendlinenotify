package com.jongzazaal.covid.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.jongzazaal.core.model.base.ErrorModel
import com.jongzazaal.covid.model.CovidToday
import com.jongzazaal.covid.repository.CovidRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch

class CovidViewModel (app: Application, var mRepository: CovidRepository): ViewModel() {
    /**
     * Factory for constructing DevByteViewModel with parameter
     */
    class Factory(val app: Application, val mRepository: CovidRepository) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(CovidViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return CovidViewModel(app, mRepository ) as T
            }
            throw IllegalArgumentException("Unable to construct viewmodel")
        }
    }

    /**
     * This is the job for all coroutines started by this ViewModel.
     *
     * Cancelling this job will cancel all coroutines started by this ViewModel.
     */
    private val viewModelJob = SupervisorJob()

    /**
     * This is the main scope for all coroutines launched by MainViewModel.
     *
     * Since we pass viewModelJob, you can cancel all coroutines launched by uiScope by calling
     * viewModelJob.cancel()
     */
    private val viewModelScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    /**
     * The data source this ViewModel will fetch results from.
     */



    /**
     * A playlist of videos that can be shown on the screen. Views should use this to get access
     * to the data.
     */

    val covidTodayModel: LiveData<CovidToday>
        get() = mRepository.covidTodayModel

    val error: LiveData<ErrorModel>
        get() = mRepository.error

    /**
     * Refresh data from the repository. Use a coroutine launch to run in a
     * background thread.
     */
    fun getCovidToday() {
        viewModelScope.launch {
            mRepository.getCovidToday()
        }
    }
    fun getCovidTodayEasySunday() {
        viewModelScope.launch {
            mRepository.getCovidTodayEasySunday()
        }
    }


}